package ru.t1.nikitushkina.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IAuthService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.api.service.ISessionService;
import ru.t1.nikitushkina.tm.api.service.IUserService;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.exception.user.AccessDeniedException;
import ru.t1.nikitushkina.tm.exception.user.LoginEmptyException;
import ru.t1.nikitushkina.tm.exception.user.PasswordEmptyException;
import ru.t1.nikitushkina.tm.model.Session;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.util.CryptUtil;
import ru.t1.nikitushkina.tm.util.HashUtil;

import javax.naming.AuthenticationException;
import java.util.Date;

@Getter
@Setter
public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @SneakyThrows
    public User check(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return user;
    }

    @NotNull
    private Session createSession(@NotNull final User user) {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return sessionService.add(session);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final User user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    @Override
    @SneakyThrows
    public String login(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AuthenticationException();
        if (user.getLocked()) throw new AuthenticationException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AuthenticationException();
        if (!hash.equals(user.getPasswordHash())) throw new AuthenticationException();
        return getToken(user);
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull Session session = objectMapper.readValue(json, Session.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        @NotNull final int timeout = getPropertyService().getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        return session;

    }

}
