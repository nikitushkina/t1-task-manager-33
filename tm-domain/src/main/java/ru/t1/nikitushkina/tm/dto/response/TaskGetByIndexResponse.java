package ru.t1.nikitushkina.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Task;

public class TaskGetByIndexResponse extends AbstractTaskResponse {

    public TaskGetByIndexResponse(@Nullable Task task) {
        super(task);
    }

}
