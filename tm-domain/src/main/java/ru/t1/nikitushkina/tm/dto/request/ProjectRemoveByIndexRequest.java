package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ProjectRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectRemoveByIndexRequest(@Nullable String token) {
        super(token);
    }

}
