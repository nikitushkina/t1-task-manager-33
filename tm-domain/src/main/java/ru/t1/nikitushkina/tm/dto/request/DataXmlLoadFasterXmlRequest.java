package ru.t1.nikitushkina.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class DataXmlLoadFasterXmlRequest extends AbstractUserRequest {

    public DataXmlLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
