package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class ProjectCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectCompleteByIdRequest(@Nullable String token) {
        super(token);
    }

}
