package ru.t1.nikitushkina.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.model.Task;

import java.util.List;

public class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private final List<Task> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

}
