package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class UserLoginRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(@Nullable String token) {
        super(token);
    }

}
